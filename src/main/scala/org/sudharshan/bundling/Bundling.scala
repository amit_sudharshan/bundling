package org.sudharshan.bundling

import java.util.Random
import collection.JavaConversions._

object Bundling {
  val cachedB = scala.collection.mutable.Map[(Double, Int, Double, Double), Double]()
}

class Bundling(gamma: Double, c: Int) {


  def p(t: Double, n: Double): Double = {
    2 * t / ((n - 1) * n)
  }

  def b(t: Double, n: Double, toPrint: Boolean = false): Double = {
    val toReturn = {
      //for t [1,c] and n [t+1, c+1]
      if (t >= 1 && t <= c && n >= t + 1 && n <= c + 1) {
        1.0
      }
      else if (n - t == 1) {
        2 * (1 - gamma) / (t + 1)
      } else {
        val j = n - t
        var sum = 0.0
        var i = 1.0
        while (i <= j - 1.0) {
          sum = sum + p(t + 1, t + j) * Bundling.cachedB.getOrElse((gamma, c, t, t + i), b(t, t + i))
          i += 1
        }
        (1 - gamma) * p(t, t + j) + gamma * (1 - gamma) * sum
      }
    }
    Bundling.cachedB.put((gamma, c, t, n), toReturn)
    toReturn
  }

  def a(t: Double, n: Double): Double = {
    var v = t + 1
    var sum = 0.0
    while (v <= n) {
      val left = (1 - gamma) * c * p(t, v)
      var i = 1
      var innerSum = 0.0
      while (i <= (v - t - 1)) {
        innerSum = innerSum + p(t + i, v) * b(t, t + i)
        i += 1
      }
      v += 1
      sum = sum + (left + innerSum)
    }
    sum
  }

  var toPrint = true

  def simulateBundle(maxN: Int, rGen: Random, withReplacement: Boolean): Seq[BundleList] = {
    val bundling = new Bundling(gamma, c)
    (c).to(maxN).map(bundle => {
      val allBtn = (0).until(bundle).map(potential => BTN(gamma, c, potential, bundle, bundling.b(potential, bundle)))
      val chosenSet = new java.util.HashSet[BTN]()
      var sizeToCheck = 0
      while (chosenSet.size() < c) {
        val filtered = if (withReplacement) allBtn else allBtn.filterNot(b => chosenSet.contains(b))
        val sum = filtered.foldLeft(0.0)(_ + _.btn)
        val btnRatio = filtered.map(b => (b, b.btn / sum)).sortBy(-1 * _._2)
        var i = 0.0
        val roulette = btnRatio.map(b => {
          i = i + b._2
          (b._1, i)
        })
        val rand = rGen.nextDouble()
        val winner = roulette.find(b => rand < b._2).getOrElse(roulette.last)
        chosenSet.add(winner._1)
        if (withReplacement) sizeToCheck += 1 else sizeToCheck = chosenSet.size()
      }
      BundleList(bundle, chosenSet.toList)
    })
  }
}

case class BTN(gamma: Double, c: Int, t: Double, n: Double, btn: Double)

case class PTN(gamma: Double, c: Int, t: Double, n: Double, ptn: Double)
