package org.sudharshan.bundling

import collection.JavaConversions._
import java.io._
import scala.collection.mutable.ArrayBuffer
import scalax.chart.Charting._

object PlotTestParams {
  val allN = Array(100)
  val allCMultipler = Array(.1)
  val gammas = Array(.01, .05, .1, .2, .4, .8, .9)
}

/**
 * The following programs PlotPtn, PlotBtn, PlotAtn, are used to
 * draw the 2d curves for each function and save them as a png file.
 * The parameters are selected from PlotTestParams
 */

object PlotPtn {
  def main(args: Array[String]) {
    import PlotTestParams._
    new File("ptn").mkdir()

    println("generating ptn map")
    val allPtn = ArrayBuffer[PTN]()
    allN.foreach(endN => {
      gammas.foreach(gamma => {
        allCMultipler.foreach(cMult => {
          val c = (cMult * endN).toInt
          val bundling = new Bundling(gamma, c)
          0.until(endN).foreach(n => {
            c.until(n - 1).foreach(t => allPtn.append(PTN(gamma, c, t, n, bundling.p(t, n))))
          })
        })
      })
    })
    println("done generating ptn map")

    allPtn.groupBy(a => (a.gamma, a.c)).foreach {
      case ((gamma, c), arr) =>
        arr.groupBy(_.t).foreach {
          case (t, allPtn) =>
            val dataset = allPtn.map(a => (t, a.ptn)).toSeq.toXYSeriesCollection(s"p[t][n] for t = $t gamma $gamma c $c")
            val chart = XYLineChart(dataset)
            val outFile = s"ptn/c.$c.t.$t.gamma.$gamma.png"
            chart.saveAsPNG(outFile, (1024, 768))
        }
    }
  }
}

object PlotBtn {
  def main(args: Array[String]): Unit = {
    import PlotTestParams._
    new File("btn").mkdir()
    allN.foreach(n => {
      gammas.foreach(gamma => {
        allCMultipler.foreach(cMult => {
          val c = (cMult * n).toInt
          val outFile = s"btn/c.$c.n.$n.gamma.$gamma.png"
          val dm = new Bundling(gamma, c)
          val bTN = 1.to(n - 1).map(t => {
            (t, dm.b(t, n))
          }).toSeq
          val dataset = bTN.toXYSeriesCollection(s"b[t][n] [for n = $n gamma $gamma c $c")
          val chart = XYLineChart(dataset)
          chart.saveAsPNG(outFile, (1024, 768))
        })
      })
    })
  }
}

object PlotAtn {
  def main(args: Array[String]): Unit = {
    import PlotTestParams._
    new File("atn").mkdir()
    allN.foreach(n => {
      gammas.foreach(gamma => {
        allCMultipler.foreach(cMult => {
          val c = (cMult * n).toInt
          val outFile = s"atn/c.$c.n.$n.gamma.$gamma.png"
          val dm = new Bundling(gamma, c)
          val aTN = 1.to(n - 1).map(t => {
            (t, dm.a(t, n))
          }).toSeq
          val dataset = aTN.toXYSeriesCollection(s"a[t][n] [for n = $n gamma $gamma c $c")
          val chart = XYLineChart(dataset)
          chart.saveAsPNG(outFile, (1024, 768))
        })
      })
    })
  }
}
