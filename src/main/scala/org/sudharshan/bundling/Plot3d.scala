package org.sudharshan.bundling

import java.awt.image.BufferedImage
import java.io._
import javax.imageio.ImageIO
import javax.swing._
import org.math.plot._
import scala.collection.mutable.ArrayBuffer


/**
 * This program will use the settings from PlotTestParams and draw 3d graphs
 * representing the btns for the given params.
 */

object Plot3d {
  def main(args: Array[String]) {
    new File("3dbtn").mkdir()
    val byGammaC = getBtnArray().groupBy(b => (b.gamma, b.c))
    byGammaC.foreach {
      case ((gamma, c), allBtn) =>
        val t = allBtn.map(_.t).toArray
        val n = allBtn.map(_.n).toArray.sorted
        val btn = allBtn.map(_.btn).toArray
        println(s"there are ${t.size} t's and ${n.size} n's and ${btn.size} btns")
        val plot = new Plot3DPanel(s"BTN gamma = $gamma c = $c n = ${n.last.toInt + 1}")
        plot.rotate(-.5, 0.0)
        plot.addScatterPlot("plot", t, n, btn)
        plot.setSize(1024, 768)
        plot.setAxisLabel(0, "t")
        plot.setAxisLabel(1, "n")
        plot.setAxisLabel(2, "b[t][n]")
        val frame = new JFrame(s"BTN gamma = $gamma c = $c n = ${n.last.toInt + 1}")
        frame.setSize(1024, 768)
        frame.setContentPane(plot)
        frame.setVisible(true)
        val outFile = s"3dbtn/gamma.$gamma.c.$c.png"
        Thread.sleep(5000)
        saveJFrame(frame, outFile)
        frame.setVisible(false)
        frame.dispose()
    }
  }

  def saveJFrame(frame: JFrame, outFile: String): Unit = {
    val img = new BufferedImage(1024, 768, BufferedImage.TYPE_INT_RGB);
    val g2d = img.createGraphics()
    frame.printAll(g2d)
    g2d.dispose()
    ImageIO.write(img, "png", new File(outFile))
  }

  def getBtnArray(): Seq[BTN] = {
    val arr = ArrayBuffer[BTN]()
    import PlotTestParams._
    gammas.foreach(gamma => {
      allCMultipler.foreach(cMult => {
        allN.foreach(endN => {
          val c = (cMult * endN).toInt
          val bundling = new Bundling(gamma, c)
          c.until(endN, 1).foreach(n => {
            c.until(n - 1).foreach(t => arr.append(BTN(gamma, c, t, n, bundling.b(t, n))))
          })
        })
      })
    })
    arr.toSeq
  }
}

