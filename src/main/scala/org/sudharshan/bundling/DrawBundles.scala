package org.sudharshan.bundling

import java.awt.{Font, Color, Stroke}
import org.jfree.chart.labels.XYSeriesLabelGenerator
import org.jfree.chart.renderer.xy.{XYBarRenderer, XYLineAndShapeRenderer}
import org.jfree.chart._
import org.jfree.chart.plot.{IntervalMarker, XYPlot, PlotOrientation}
import org.jfree.data.xy.{XYDataset, IntervalXYDataset, XYSeriesCollection, XYSeries}
import org.jfree.ui._
import scala.swing._
import scala.swing.Font

object SimulateBundlesTestParams {
  val maxN = 100
  val c = 10
  val gamma = .2
  val useRatio =false
  val withReplacement = true
  val avgSimulationCount = 100
  val withReplacementTitle = if(withReplacement) "with replacement" else "without replacement"
}

/**
 * This program will run a simulation given the parameters in SimulateBundlesTestParams,
 * and draw a 2d plot of the which technologies connect
 */
object DrawBundles extends SimpleSwingApplication {

  import SimulateBundlesTestParams._

  def top = new MainFrame {
    val rGen = new java.util.Random(1)
    val bundleList = new Bundling(gamma, c).simulateBundle(maxN, rGen, withReplacement)
    val width = 1024
    val height = 768
    title = s" bundle for parameters ( gamma: $gamma c $c maxN $maxN )  $withReplacementTitle"
    contents = new DataPanel(width, height, bundleList) {
      preferredSize = new Dimension(width, height)
    }
  }
}

/**
 * This program will generate a histogram showing which bundles connect to other bundles
 * most frequently. It will layer on top of a single simulation, an averaging.
 */

object GetSimulateBundleHistogram extends ApplicationFrame("btn histogram") {

  import SimulateBundlesTestParams._

  val randomCount = getCounts
  val avg = averageCounts(0.until(avgSimulationCount).map(a => getCounts).toSeq)
  val chart = createChart(createDataset(randomCount, useRatio = useRatio), createDataset(avg, useRatio = useRatio))
  val chartPanel = new ChartPanel(chart)
  chartPanel.setPreferredSize(new java.awt.Dimension(1024, 768))
  setContentPane(chartPanel)

  def getCounts: java.util.HashMap[Int, Int] = {
    val rGen = new java.util.Random()
    val bundleList = new Bundling(gamma, c).simulateBundle(maxN, rGen, withReplacement)
    val countMap = new java.util.HashMap[Int, Int]()
    0.until(maxN).foreach(i => countMap.put(i, 0))
    bundleList.filter(b => true).foreach(b => {
      b.list.foreach(i => {
        countMap.put(i.t.toInt, countMap.get(i.t.toInt) + 1)
      })
    })
    countMap
  }

  def averageCounts(allCounts: Seq[java.util.HashMap[Int, Int]]): java.util.HashMap[Int, Int] = {
    val countMap = new java.util.HashMap[Int, Int]()
    0.until(maxN).foreach(i => countMap.put(i, 0))
    0.until(maxN).foreach(i => {
      val avg = allCounts.foldLeft(0.0)(_ + _.get(i)) / allCounts.size
      countMap.put(i, avg.toInt)
    })
    countMap
  }

  def createDataset(countMap: java.util.HashMap[Int, Int], useRatio: Boolean): IntervalXYDataset = {
    val series = new XYSeries("");
    0.until(maxN).foreach(i => {
      val ratio = if (useRatio) (maxN - i).toDouble else 1.0
      series.add(i, countMap.get(i) / ratio)
    })
    new XYSeriesCollection(series);
  }

  def createChart(dataset: IntervalXYDataset, avg: IntervalXYDataset): JFreeChart = {
    val title = if(useRatio) "Selection to Opportunity Fraction" else "Number of Initial bundles"
    val yAxisLabel = if(useRatio) "selection to opportunity fraction" else "count"
    val chart = ChartFactory.createXYBarChart(
      s"$title for gamma=${gamma} c=${c} ${withReplacementTitle}",
      "t",
      false,
      yAxisLabel,
      avg,
      PlotOrientation.VERTICAL,
      true,
      true,
      false
    );
    val plot = chart.getPlot().asInstanceOf[XYPlot];
    val randomRenderer = new XYBarRenderer() {
      override def getLegendItemLabelGenerator: XYSeriesLabelGenerator = new XYSeriesLabelGenerator {
        override def generateLabel(dataset: XYDataset, series: Int): String = "single random simulation"
      }
    }

    val avgRenderer = new XYLineAndShapeRenderer(true, true) {
      override def getLegendItemLabelGenerator: XYSeriesLabelGenerator = new XYSeriesLabelGenerator {
        override def generateLabel(dataset: XYDataset, series: Int): String = s"avg of ${avgSimulationCount} simulations"
      }
    }

    plot.setRenderer(0, avgRenderer)
    plot.setDataset(1, dataset)
    plot.setRenderer(1, randomRenderer)
    0.until(1).foreach(i => {
      plot.getDomainAxis(i).setAutoRange(true)
    })
    return chart
  }

  def main(args: Array[String]) {
    this.pack();
    RefineryUtilities.centerFrameOnScreen(this);
    this.setVisible(true);
  }
}

class DataPanel(width: Int, height: Int, bundles: Iterable[BundleList]) extends Panel {

  override def paintComponent(g: Graphics2D) {
    // lets paint the top and bottom lines 20% from the top and 20% from the bottom
    val bottomY = (height * .8).toInt
    val topY = (height * .2).toInt
    g.drawLine(0, topY, width, topY)
    g.drawLine(0, bottomY, width, bottomY)
    // lets draw some tick marks
    val minT = 0
    val maxT = (bundles.flatMap(_.list.map(_.t.toInt)).toList ::: bundles.map(_.bundle).toList).max
    val delta = (width / (maxT - minT))
    println(s"delta is $delta")
    val tickHeight = 6
    0.until(maxT).foreach(t => {
      g.drawLine(t * delta, topY + tickHeight, t * delta, topY - tickHeight)
      g.drawLine(t * delta, bottomY + tickHeight, t * delta, bottomY - tickHeight)
      g.drawString(t.toString, t * delta, bottomY + 2 * tickHeight)
      g.drawString(t.toString, t * delta, topY - 2 * tickHeight)
    })
    bundles.foreach(bundle => {
      g.setColor(Color.RED)
      val toX = bundle.bundle * delta
      bundle.list.foreach(l => {
        val fromX = l.t.toInt * delta
        g.drawLine(fromX, bottomY, toX, topY)
      })
    })
  }
}

case class BundleList(bundle: Int, list: List[BTN]) {
  override def toString: String = {
    s"bundle $bundle ${list.map(_.t).mkString(",")}"
  }
}

