name := """scalaDadMarketing"""

version := "1.0"

scalaVersion := "2.10.2"

unmanagedBase <<= baseDirectory { base => {
println("base is " + base)
base / "lib" }
}

libraryDependencies += "org.scalatest" %% "scalatest" % "1.9.1" % "test"

libraryDependencies += "com.github.wookietreiber" %% "scala-chart" % "latest.integration"

libraryDependencies += "org.tinyjee.jgraphx" % "jgraphx" % "2.0.0.1"

